<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'=>'wandypurnomo',
                'email'=>'wandypurnomo92@gmail.com',
                'password'=>bcrypt('wandx54'),
                'role' =>'admin'
            ]
        ]);

        DB::table('user_profiles')->insert([
            [
                'firstname'=>'wandy',
                'middlename'=>'',
                'lastname'=>'purnomo',
                'address'=>'beluran sidomoyo godean sleman yogyakarta',
                'avatar'=>'uploads/avatar/logo-xs.png',
                'user_id'=>1
            ]
        ]);
    }
}
