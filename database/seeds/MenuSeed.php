<?php

use Illuminate\Database\Seeder;

class MenuSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
            [
                'name'=>'dashboard',
                'display_name'=>'Dashboard',
                'icon'=>'fa fa-dashboard'
            ],
            [
                'name'=>'kost',
                'display_name'=>'Kost',
                'icon'=>'fa fa-th'
            ]
        ]);

        DB::table('sub_menus')->insert([
            [
                'name'=>'add_kost',
                'display_name'=>'Add Kost',
                'icon'=>'fa fa-plus',
                'menu_id'=>2
            ],
            [
                'name'=>'lists',
                'display_name'=>'Lists Kost',
                'icon'=>'fa fa-tasks',
                'menu_id'=>2
            ]
        ]);
    }
}
