<?php

use Illuminate\Database\Seeder;

class UserMenuSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu_user')->insert([
            [
                'user_id' => 1,
                'menu_id' => 1
            ],
            [
                'user_id'=>1,
                'menu_id'=>2
            ]
        ]);
    }
}
