<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoleSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles',function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('role');
        });

        Schema::create('role_user',function(Blueprint $tb){
            $tb->integer('role_id')->unsigned();
            $tb->integer('user_id')->unsigned();

            $tb->foreign('role_id')->references('id')->on('roles')->onDelete('cascade')->onUpdate('cascade');
            $tb->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
