<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKostSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kosts',function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('name');
            $tb->text('address');
            $tb->enum('gender',['male','female','mix']);
            $tb->integer('price')->unsigned();
            $tb->string('latitude');
            $tb->string('longitude');
            $tb->integer('available')->unsigned();
            $tb->integer('user_id')->unsigned();
            $tb->timestamps();

            $tb->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('facilities',function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('name');
        });

        Schema::create('facility_kost',function(Blueprint $tb){
            $tb->integer('kost_id')->unsigned();
            $tb->integer('facility_id')->unsigned();

            $tb->foreign('kost_id')->references('id')->on('kosts')->onDelete('cascade')->onUpdate('cascade');
            $tb->foreign('facility_id')->references('id')->on('facilities')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('types',function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('type');
            $tb->integer('value')->unsigned();
        });

        Schema::create('kost_type',function(Blueprint $tb){
            $tb->integer('kost_id')->unsigned();
            $tb->integer('type_id')->unsigned();

            $tb->foreign('kost_id')->references('id')->on('kosts')->onDelete('cascade')->onUpdate('cascade');
            $tb->foreign('type_id')->references('id')->on('types')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('pictures',function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('path');
            $tb->string('filename');
            $tb->enum('is_primary',[0,1])->default(0);
            $tb->timestamps();
        });

        Schema::create('kost_picture',function(Blueprint $tb){
            $tb->integer('kost_id')->unsigned();
            $tb->integer('picture_id')->unsigned();

            $tb->foreign('kost_id')->references('id')->on('kosts')->onDelete('cascade')->onUpdate('cascade');
            $tb->foreign('picture_id')->references('id')->on('pictures')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kosts');
        Schema::drop('facilities');
        Schema::drop('facility_kost');
        Schema::drop('types');
        Schema::drop('kost_type');
        Schema::drop('pictures');
        Schema::drop('kost_picture');
    }
}
