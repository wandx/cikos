<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRoleSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('role_user',function(Blueprint $tb){
            $tb->dropForeign('role_user_role_id_foreign');
            $tb->dropForeign('role_user_user_id_foreign');
        });

        Schema::drop('role_user');
        Schema::drop('roles');

        Schema::table('users',function(Blueprint $tb){
            $tb->enum('role',['admin','user','vendor'])->default('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function(Blueprint $tb){
            $tb->dropIfExists('role');
        });
    }
}
