<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterKost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kosts',function(Blueprint $tb){
            $tb->text('desc');
            $tb->dropColumn('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kosts',function(Blueprint $tb){
            $tb->integer('price')->unsigned();
            $tb->dropColumn('desc');
        });
    }
}
