<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create user profile
        Schema::create('user_profiles',function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('firstname',50);
            $tb->string('middlename',50);
            $tb->string('lastname',50);
            $tb->text('address');
            $tb->string('avatar');
            $tb->integer('user_id')->unsigned();

            $tb->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

        // create contact
        Schema::create('contacts',function(Blueprint $tb){
            $tb->increments('id');
            $tb->enum('contact_type',['phone','home','fax','office'])->default('phone');
            $tb->string('contact_number');
        });

        // create contact_user_profile
        Schema::create('contact_user_profile',function(Blueprint $tb){
            $tb->integer('user_profile_id')->unsigned();
            $tb->integer('contact_id')->unsigned();

            $tb->foreign('user_profile_id')->references('id')->on('user_profiles')->onDelete('cascade')->onUpdate('cascade');
            $tb->foreign('contact_id')->references('id')->on('contacts')->onDelete('cascade')->onUpdate('cascade');
        });

        // create social_media
        Schema::create('social_medias',function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('socmed_name');
            $tb->string('socmed_value');
        });

        // create social_media_user_profile
        Schema::create('social_media_user_profile',function(Blueprint $tb){
            $tb->integer('user_profile_id')->unsigned();
            $tb->integer('social_media_id')->unsigned();

            $tb->foreign('user_profile_id')->references('id')->on('user_profiles')->onDelete('cascade')->onUpdate('cascade');
            $tb->foreign('social_media_id')->references('id')->on('social_medias')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
        Schema::dropIfExists('contacts');
        Schema::dropIfExists('social_medias');
        Schema::dropIfExists('contact_user_profile');
        Schema::dropIfExists('social_media_user_profile');
    }
}
