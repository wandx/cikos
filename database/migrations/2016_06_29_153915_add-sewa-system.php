<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSewaSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rents',function(Blueprint $tb){
            $tb->increments('id');
            $tb->enum('jenis_sewa',['harian','bulanan','tahunan']);
            $tb->integer('periode')->unsigned();
            $tb->integer('harga')->unsigned();
        });

        Schema::create('kost_rent',function(Blueprint $tb){
            $tb->integer('kost_id')->unsigned();
            $tb->integer('rent_id')->unsigned();

            $tb->foreign('kost_id')->references('id')->on('kosts')->onDelete('cascade')->onUpdate('cascade');
            $tb->foreign('rent_id')->references('id')->on('rents')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rents');
    }
}
