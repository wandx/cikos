<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MenuSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create menus
        Schema::create('menus',function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('name',50);
            $tb->string('display_name',50);
            $tb->string('icon');
        });

        // create sub_menus
        Schema::create('sub_menus',function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('name',50);
            $tb->string('display_name',50);
            $tb->string('icon');
            $tb->integer('menu_id')->unsigned();

            $tb->foreign('menu_id')->references('id')->on('menus')->onDelete('cascade')->onUpdate('cascade');
        });

        // relation menu_user
        Schema::create('menu_user',function(Blueprint $tb){
            $tb->integer('menu_id')->unsigned();
            $tb->integer('user_id')->unsigned();

            $tb->foreign('menu_id')->references('id')->on('menus')->onDelete('cascade')->onUpdate('cascade');
            $tb->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExixts('menus');
        Schema::dropIfExixts('sub_menus');
        Schema::dropIfExixts('menu_user');
    }
}
