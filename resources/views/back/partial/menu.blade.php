
<ul class="sidebar-menu">
    {{--loop user--}}
    @foreach($user as $u)
        {{--loop menu inside user--}}
        @foreach($u->menu as $m)
            {{--check sub menu availability--}}
            @if($m->sub_menu->count() > 0)
                {{--sub menu exists--}}
                <li class="treeview {{ (Request::segment(2) == $m->name || $m->name === 'dashboard') ? 'active' : '' }}" >
                    <a href="#">
                        <i class="{{$m->icon}}"></i> <span>{{$m->display_name}}</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @foreach($m->sub_menu as $sm)
                            <li class="{{ (Request::segment(3) == $sm->name) ? 'active' : '' }}">
                                <a href="{{url('backoffice/'.$m->name.'/'.$sm->name)}}">
                                    <i class="{{$sm->icon}}"></i> <span>{{$sm->display_name}}</span>
                                </a>
                            </li>
                        @endforeach

                    </ul>
                </li>

            @else
                {{--sub menu doesn't exists--}}
                <li class ="{{ (Request::segment(2) == $m->name || (Request::segment(3)) == '' && $m->name == 'dashboard') ? 'active' : '' }}">
                    <a href="{{route('bo.'.$m->name)}}">
                        <i class="{{$m->icon}}"></i>
                        <span>{{$m->display_name}}</span>

                        {{--reserve place for sms notification--}}
                        @if($m->name == "sms")
                            {{--if notification exist--}}
                            <span class="label label-primary pull-right sms-notification {{(\App\Libraries\Admin::check_sms_notification()) ? '':'hidden'}}">{{DB::table('sections')->where('name','sms_notification')->first()->content}}</span>
                        @endif
                    </a>
                </li>
            @endif
        @endforeach


    @endforeach
</ul>
