@extends('back._master')
@section('custom_css')
    <style>
        #add-contact,
        #add-sewa{
            display: flex;
            flex-direction:row;
            justify-content: center;
            align-items: center;
            background-color: #eee;
            height:180px;
            transition: background-color ease 1s 0s;
        }

        #add-sewa{
            height:187px;
        }

        #add-contact:hover,
        #add-sewa:hover{
            background-color: #a6e1ec;;
        }

        #add-contact button,
        #add-sewa button{
            border-radius:100%;
        }

        .contact-container,
        .sewa-container{
            border:1px solid #ddd;
            padding:15px;
        }

        .contact-container,
        #add-contact,
        .sewa-container,
        #add-sewa{
            margin-bottom:20px;
        }
    </style>    
@stop
@section('content_caption')
    <h3>
        Add Kost
        <small>Tambah kost</small>
    </h3>

@stop
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if(count($errors) > 0)
                        <div class="login-alert">
                            @foreach($errors->all() as $e)
                                <li>{{$e}}</li>
                            @endforeach
                        </div>
                    @endif
                    {!! Form::open(['route'=>'bo.kost.add.post','id'=>'add-kost-form']) !!}
                    <div class="col-sm-12">
                        <div class="row">
                            <h2 class="text-center">Informasi dasar</h2>
                            <hr>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="">Nama Kost</label>
                                    <input type="text" name="nama_kost" required class="form-control" oninvalid="setCustomValidity('Nama kost harus diisi')">
                                </div>
                                <div class="form-group">
                                    <label for="">Provinsi</label>
                                    <select name="prov" id="prov" class="form-control select2" required  oninvalid="setCustomValidity('Provinsi harus diisi')">
                                        {!! Front::get_prov() !!}
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Kabupaten</label>
                                    <select disabled name="kabupaten" id="kabupaten" class="form-control select2" oninvalid="setCustomValidity('Kabupaten harus diisi')">
                                        <option value="">Pilih Kabupaten</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Kecamatan</label>
                                    <select name="kecamatan" disabled id="kecamatan" class="form-control select2" oninvalid="setCustomValidity('Kecamatan harus diisi')">
                                        <option value="">Pilih Kecamatan</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="">Desa</label>
                                    <select name="desa" disabled id="desa" class="form-control select2" oninvalid="setCustomValidity('Desa harus diisi')">
                                        <option value="">Pilih Desa</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Alamat</label>
                                    <textarea name="alamat" id="alamat" cols="30" rows="4" class="form-control" required oninvalid="setCustomValidity('alamat harus diisi')"></textarea>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="">Gender</label>
                                    <div class="radio-group">
                                        <label for="" class="radio-inline">
                                            <input checked type="radio" name="gender" value="male"> Laki - laki
                                        </label>
                                        <label for="" class="radio-inline">
                                            <input type="radio" name="gender" value="female"> Perempuan
                                        </label>
                                        <label for="" class="radio-inline">
                                            <input type="radio" name="gender" value="mix"> Campur
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Latitude & Longitude</label>
                                    <div class="row">
                                        <div class="col-sm-6"><input type="text" name="latitude" class="form-control" placeholder="Latitude"></div>
                                        <br class="visible-xs">
                                        <div class="col-sm-6"><input type="text" name="longitude" class="form-control" placeholder="Longitude"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Deskripsi</label>
                                    <textarea name="desc" id="desc" rows="15" required class="form-control" oninvalid="setCustomValidity('Deskripsi harus diisi')"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h2 class="text-center">Kontak</h2>
                            <hr>
                            <div id="all-contact-container">
                                <div class="col-sm-6">
                                    <div class="contact-container">
                                        <div class="form-group">
                                            <label for="">Jenis kontak</label>
                                            <select name="jenis-kontak[]"  class="form-control select2" required>
                                                <option value="phone">Handphone</option>
                                                <option value="home">Telefon Rumah</option>
                                                <option value="office">Kantor</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Nomer</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    +62
                                                </span>
                                                <input type="number" name="nomer[]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div id="add-contact">
                                    <button class="btn btn-info btn-lg"><i class="fa fa-plus fa-2x"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h2 class="text-center">Harga dan sewa</h2>
                            <hr>
                            <div id="all-sewa-container">
                                <div class="col-sm-6">
                                    <div class="sewa-container">
                                        <div class="form-group">
                                            <label for="">Jenis Sewa</label>
                                            <select name="jenis-sewa[]"  class="form-control select2" required>
                                                <option value="harian">Harian</option>
                                                <option value="bulanan" selected>Bulanan</option>
                                                <option value="tahunan">Tahunan</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Harga / Periode</label>
                                            <div class="row">
                                                <div class="col-sm-6" style="padding-right: 0;">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">Rp.</span>
                                                        <input type="number" required class="form-control" name="harga[]" placeholder="Harga">
                                                    </div>
                                                </div>
                                                <div class="col-sm-1 text-center" style="padding: 0;">
                                                    <span style="font-size: 2.1em;">/</span>
                                                </div>
                                                <div class="col-sm-5" style="padding-left: 0;">
                                                    <div class="input-group">
                                                        <input type="number" required name="periode[]" class="form-control" placeholder="Periode">
                                                        <div class="input-group-addon periode-addon">Bulan</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div id="add-sewa">
                                    <button class="btn btn-info btn-lg"><i class="fa fa-plus fa-2x"></i></button>
                                </div>

                            </div>
                        </div>
                    </div>



                    <div class="col-sm-12 text-right">
                        <button class="btn btn-primary">Next <i class="fa fa-chevron-circle-right"></i></button>
                    </div>
                    {!! Form::close() !!}

                    {{--Master--}}
                    <div class="hidden" id="master-contact">
                        <div class="col-sm-6">
                            <div class="contact-container">
                                <button class="close pull-right hapus-kontak-form" style="margin-top: -14px;margin-right: -10px;" data-toggle="tooltip" data-placement="top" title="Hapus">&times;</button>
                                <div class="form-group">
                                    <label for="">Jenis kontak</label>
                                    <select name="jenis-kontak[]"  class="form-control select2" required>
                                        <option value="phone">Handphone</option>
                                        <option value="home">Telefon Rumah</option>
                                        <option value="office">Kantor</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Nomer</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            +62
                                        </span>
                                        <input type="number" name="nomer[]" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hidden" id="master-sewa">
                        <div class="col-sm-6">
                            <div class="sewa-container">
                                <button class="close pull-right hapus-sewa-form" style="margin-top: -14px;margin-right: -10px;"  data-toggle="tooltip" data-placement="top" title="Hapus">&times;</button>
                                <div class="form-group">
                                    <label for="">Jenis Sewa</label>
                                    <select name="jenis-sewa[]"  class="form-control select2" required>
                                        <option value="harian">Harian</option>
                                        <option value="bulanan" selected>Bulanan</option>
                                        <option value="tahunan">Tahunan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Harga / Periode</label>
                                    <div class="row">
                                        <div class="col-sm-6" style="padding-right: 0;">
                                            <div class="input-group">
                                                <span class="input-group-addon">Rp.</span>
                                                <input type="number" required class="form-control" name="harga[]" placeholder="Harga">
                                            </div>
                                        </div>
                                        <div class="col-sm-1 text-center" style="padding: 0;">
                                            <span style="font-size: 2.1em;">/</span>
                                        </div>
                                        <div class="col-sm-5" style="padding-left: 0;">
                                            <div class="input-group">
                                                <input type="number" required name="periode[]" class="form-control" placeholder="Periode">
                                                <div class="input-group-addon periode-addon">Bulan</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="hidden" id="master-btn-close-kontak">
                        <button class="close pull-right hapus-kontak-form" style="margin-top: -14px;margin-right: -10px;" data-toggle="tooltip" data-placement="top" title="Hapus">&times;</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('custom_script')
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/SimpleAjaxUploader.min.js')}}"></script>
    <script>
        $('.select2').select2();

        // Daerah

        // when provinsi changed
        $('#prov').change(function(){
            if($(this).val() != ""){
                $('#kabupaten').prop('disabled',false);
                $.get("{{url('backoffice/kost/get_kabupaten')}}/"+$(this).val(),function(data){
                    $('#kabupaten').html(data).val("").trigger('change');
                });
            }else{
                $('#kabupaten').prop('disabled',true).val("").trigger('change');
            }
        });

        // when kabupaten changed
        $('#kabupaten').change(function () {
            if($(this).val() != ""){
                $('#kecamatan').prop('disabled',false);
                $.get("{{url('backoffice/kost/get_kecamatan')}}/"+$(this).val(),function(data){
                    $('#kecamatan').html(data).val("").trigger('change');
                });
            }else{
                $('#kecamatan').prop('disabled',true).val("").trigger('change');
            }
        });

        // when kecamatan changed
        $('#kecamatan').change(function () {
            if($(this).val() != ""){
                $('#desa').prop('disabled',false);
                $.get("{{url('backoffice/kost/get_desa')}}/"+$(this).val(),function(data) {
                    $('#desa').html(data).val("").trigger('change');
                });
            }else{
                $('#desa').prop('disabled',true).val("").trigger('change');
            }
        });

        // tambah kontak form
        $(document).on("click","#add-contact button",function(e){
            e.preventDefault();
            $("select").select2("destroy");
            $("#all-contact-container").append($("#master-contact").html());
            $("select").select2();
            // jika banyak kontak form lebih dari atau sama dengan 4
            var jfk = $("#all-contact-container select[name='jenis-kontak[]']").length
            if(jfk >= 4){
                $("#add-contact").parent().addClass("hidden");
            }else{
                $("#add-contact").parent().removeClass("hidden");
            }

            if(jfk > 1 && !cek_btn_kontak()){
                $("#all-contact-container .col-sm-6:nth-child(1) .contact-container").prepend($("#master-btn-close-kontak").html());
            }else{
                $("#all-contact-container .col-sm-6:nth-child(1) .contact-container").find(".hapus-kontak-form").remove();
            }
        })

        // cek button hapus kontak ada
        function cek_btn_kontak() {
            var x = $("#all-contact-container .col-sm-6:nth-child(1) .contact-container").find(".hapus-kontak-form").length;
            if(x > 1){
                return true;
            }else{
                return false;
            }

        }

        // tambah sewa form
        $("#add-sewa button").click(function(e){
            e.preventDefault();
            $("select").select2("destroy");
            $("#all-sewa-container").append($("#master-sewa").html());
            $("select").select2();
            // jika banyak kontak form lebih dari atau sama dengan 4
            var jfk = $("#all-sewa-container select[name='jenis-sewa[]']").length
            if(jfk >= 4){
                $("#add-sewa").parent().addClass("hidden");
            }else{
                $("#add-sewa").parent().removeClass("hidden");
            }
        });

        // hapus kontak form
        $(document).on("click",".hapus-kontak-form",function(e){
            e.preventDefault();
            $(this).parent().parent().remove();
            // jika banyak kontak form lebih dari atau sama dengan 4
            var jfk = $("#all-contact-container select[name='jenis-kontak[]']").length
            if(jfk >= 4){
                $("#add-contact").parent().addClass("hidden");
            }else{
                $("#add-contact").parent().removeClass("hidden");
            }

            if(jfk > 1 && cek_btn_kontak()){
                $("#all-contact-container .col-sm-6:nth-child(1) .contact-container").prepend($("#master-btn-close-kontak").html());
            }else{
                $("#all-contact-container .col-sm-6:nth-child(1) .contact-container").find(".hapus-kontak-form").remove();
            }
        })

        // hapus sewa form
        $(document).on("click",".hapus-sewa-form",function(e){
            e.preventDefault();
            $(this).parent().parent().remove();
            // jika banyak kontak form lebih dari atau sama dengan 4
            var jfk = $("#all-sewa-container select[name='jenis-sewa[]']").length
            if(jfk >= 4){
                $("#add-sewa").parent().addClass("hidden");
            }else{
                $("#add-sewa").parent().removeClass("hidden");
            }
        })

        // Jenis sewa UX
        $(document).on("change","select[name='jenis-sewa[]']",function(){
            var elem = $(this);
            if(elem.val() == "harian"){
                $(this).parent().parent().find(".periode-addon").html("Hari");
            }else if(elem.val() == "bulanan"){
                $(this).parent().parent().find(".periode-addon").html("Bulan");
            }else{
                $(this).parent().parent().find(".periode-addon").html("Tahun");
            }

        })

    </script>
@stop