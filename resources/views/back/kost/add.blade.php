@extends('back._master')
@section('custom_css')
    <style>
        .holder-container{
            position: relative;
            display: block;
            z-index:1;
        }

        .img-container{
            display: block;
            position: relative;
            clear: both;
            width:100%;
            height:inherit;
            z-index:0;
        }

        .img-container img{
            position: relative;
            display: inline-block;
            width:100%;
            z-index: 1;
        }

        .up-overlay{
            position: absolute;
            width:100%;
            height:100%;
            background: rgba(1,1,1,.7);
            z-index:100;
            display: flex;
            flex-direction:row;
            justify-content:center;
            align-items:center;
            color: #fff;
        }
    </style>
@endsection
@section('content_caption')
    <h3>
        Add Kost
    </h3>
@endsection
@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active" role="presentation">
                        <a href="#tab1" role="tab" aria-controls="tab1" data-toggle="tab">
                            <i class="fa fa-home"></i> Informasi dasar
                        </a>
                    </li>

                    <li class="" role="presentation">
                        <a href="#tab2" role="tab" aria-controls="tab2" data-toggle="tab">
                            <i class="fa fa-feed"></i> Tentukan Fasilitas & Harga
                        </a>
                    </li>

                    <li class="" role="presentation">
                        <a href="#tab3" role="tab" aria-controls="tab3" data-toggle="tab">
                            <i class="fa fa-image"></i> Tambahkan Gambar
                        </a>
                    </li>
                </ul>
                {!! Form::open(['route'=>'bo.kost.add.post','files'=>'true','id'=>'add-kost-form']) !!}
                <div class="tab-content">

                    <div class="tab-pane active" id="tab1" role="tabpanel">
                        <br>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">Nama Kost</label>
                                <input type="text" name="nama_kost" required class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Provinsi</label>
                                <select name="prov" id="prov" class="form-control select2">
                                    {!! Front::get_prov() !!}
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Kabupaten</label>
                                <select disabled name="kabupaten" id="kabupaten" class="form-control select2">
                                    <option value="">Pilih Kabupaten</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Kecamatan</label>
                                <select name="kecamatan" disabled id="kecamatan" class="form-control select2">
                                    <option value="">Pilih Kecamatan</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Desa</label>
                                <select name="desa" disabled id="desa" class="form-control select2">
                                    <option value="">Pilih Desa</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Alamat</label>
                                <textarea name="alamat" id="alamat" cols="30" rows="4" class="form-control"></textarea>
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">Gender</label>
                                <div class="radio-group">
                                    <label for="" class="radio-inline">
                                        <input type="radio" name="gender"> Laki - laki
                                    </label>
                                    <label for="" class="radio-inline">
                                        <input type="radio" name="gender"> Perempuan
                                    </label>
                                    <label for="" class="radio-inline">
                                        <input type="radio" name="gender"> Campur
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">No.telf</label>
                                <div class="input-group">
                                    <span class="input-group-addon">+62</span>
                                    <input type="number" name="nope" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Latitude & Longitude</label>
                                <div class="row">
                                    <div class="col-sm-6"><input type="text" name="latitude" class="form-control" placeholder="Latitude"></div>
                                    <br class="visible-xs">
                                    <div class="col-sm-6"><input type="text" name="longitude" class="form-control" placeholder="Longitude"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Deskripsi</label>
                                <textarea name="desc" id="desc" rows="12" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2" role="tabpanel">
                        <br>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">Fasilitas <small data-toggle="modal" data-target="#add-fasilitas" class="label label-info" style="cursor: pointer;">Tambah fasilitas</small></label>
                                <select data-placeholder="pilih fasilitas" name="fasilitas[]" multiple="multiple" id="fasilitas" class="form-control select2" style="width:100%">
                                    {!! Front::fasilitas() !!}
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div id="container-jenis-sewa">
                                <div class="well">
                                    <div class="form-group">
                                        <label for="">Jenis Sewa</label>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <select name="jenis[]" class="form-control jenis" style="width:100%" tabindex="-1" aria-hidden="true">
                                                    <option value="bulanan">Bulanan</option>
                                                    <option value="tahunan">Tahunan</option>
                                                    <option value="harian">Harian</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon">/</span>
                                                    <input type="number" name="jenis_value[]" class="form-control jenis-value">
                                                    <span class="input-group-addon lbl">bulan</span>
                                                </div>
                                            </div>
                                            &nbsp;
                                            <div class="col-sm-12">
                                                <input type="number" name="harga_sewa[]" placeholder="Harga / bulan" class="form-control harga-sewa">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button class="btn btn-primary" id="add-jenis"><i class="fa fa-plus"></i> Tambah Jenis Sewa</button>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab3" role="tabpanel">
                        <br>
                        <div class="col-sm-4">
                            <div class="holder-container">
                                <div class="up-overlay">
                                    <i class="fa fa-upload fa-3x" id="up-btn1" style="cursor: pointer;"></i>
                                    <progress id="p1" style="display: none;"></progress>
                                </div>
                                <div class="img-container clearfix">
                                    <img src="{{asset('assets/img/img_holder.png')}}" alt="upload image" class="img img-rounded">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="holder-container">
                                <div class="up-overlay">
                                    <i class="fa fa-upload fa-3x" id="up-btn2" style="cursor: pointer;"></i>

                                </div>
                                <div class="img-container clearfix">
                                    <img src="{{asset('assets/img/img_holder.png')}}" alt="upload image" class="img img-rounded">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="holder-container">
                                <div class="up-overlay">
                                    <i class="fa fa-upload fa-3x" id="up-btn3" style="cursor: pointer;"></i>
                                    <canvas id="canvas3" width="100" height="100" class="hidden"></canvas>
                                </div>
                                <div class="img-container clearfix">
                                    <img src="{{asset('assets/img/img_holder.png')}}" alt="upload image" class="img img-rounded">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <input type="submit" class="btn btn-lg btn-primary" value="add">
                        </div>
                    </div>

                </div>
                {!! Form::close() !!}
                <div class="row">

                </div>
            </div>
        </div>
    </div>
    <input type="file" name="images" id="images" style="display: none;">
    <div class="hidden" id="master-jenis">
        <div class="well">
            <button class="close pull-right hapus-jenis" style="margin-top: -15px;margin-right: -15px;">&times;</button>
            <div class="form-group">
                <label for="">Jenis Sewa</label>
                <div class="row">
                    <div class="col-sm-6">
                        <select name="jenis[]" class="form-control jenis" style="width:100%" tabindex="-1" aria-hidden="true">
                            <option value="bulanan">Bulanan</option>
                            <option value="tahunan">Tahunan</option>
                            <option value="harian">Harian</option>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <span class="input-group-addon">/</span>
                            <input type="number" name="jenis_value[]" class="form-control jenis-value">
                            <span class="input-group-addon lbl">bulan</span>
                        </div>
                    </div>
                    &nbsp;
                    <div class="col-sm-12">
                        <input type="number" name="harga_sewa[]" placeholder="Harga / bulan" class="form-control harga-sewa">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modals')
    <!-- Modal -->
    <div id="add-fasilitas" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah fasilitas</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="nama-fasilitas" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-info" id="btn-add-fasilitas"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@stop
@section('custom_script')
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/SimpleAjaxUploader.min.js')}}"></script>
    <script>
        $('.select2').select2();

        // tambah jenis sewa
        $('#add-jenis').click(function(e){
            e.preventDefault();
            $('#container-jenis-sewa').append($('#master-jenis').html());
            var banyak_form = $('#container-jenis-sewa .well').length;
            if(banyak_form >= 3){
                $(this).prop('disabled',true);
            }
        });

        // hapus jenis
        $(document).on('click','.hapus-jenis',function(){
            $(this).parent().remove();
            var banyak_form = $('#container-jenis-sewa .well').length;
            if(banyak_form < 3){
                $('#add-jenis').prop('disabled',false);
            }


        });

        function progress(elem){
            var cv = document.getElementById(elem).getContext('2d'), // canvas id selector
                    al = 0, // amount loaded
                    start = 4.72, // start amount
                    cw = cv.canvas.width, // canvas width
                    ch = cv.canvas.height, // canvas height
                    diff;
            function progressSim(){
                diff = ((al / 100) * Math.PI*2*10).toFixed(2);
                cv.clearRect(0,0,cw,ch);
                cv.lineWidth = 0.16*cw;
                cv.fillStyle = "#fff";
                cv.strokeStyle = "#fff";
                cv.textAlign = "center";
                cv.fillText(al+'%',cw/2,(ch/2)+2,cw);
                cv.beginPath();
                cv.arc(cw/2,ch/2,(cw/2)*0.66,start,(diff/10)+start,false);
                cv.stroke();
                if(al >= 100){
                    clearTimeout(sim);
                }
                al++;
            }
            var sim = setInterval(progressSim,50);
        }
        $(document).ready(function(){
            var progress = $('#p1');
            var up1 = new ss.SimpleUpload({
                customHeaders : {
                    'X-CSRF-TOKEN': "{!! csrf_token() !!}"
                },
                button:'up-btn1',
                multipart:true,
                responseType : 'json',
                name : 'img',
                url : '{{route("bo.kost.upload")}}',
                onSubmit:function(filename,ext){
                    this.setProgressBar(progress);
                },
                onComplete:function (filename,response) {
                    if(!response){
                        alert('failed');
                        return false;
                    }
                }
            });
        });


        // daerah
        // when provinsi changed

        $('#prov').change(function(){
            if($(this).val() != ""){
                $('#kabupaten').prop('disabled',false);
                $.get("{{url('backoffice/kost/get_kabupaten')}}/"+$(this).val(),function(data){
                    $('#kabupaten').html(data).val("").trigger('change');
                });
            }else{
                $('#kabupaten').prop('disabled',true).val("").trigger('change');
            }
        });

        // when kabupaten changed
        $('#kabupaten').change(function () {
            if($(this).val() != ""){
                $('#kecamatan').prop('disabled',false);
                $.get("{{url('backoffice/kost/get_kecamatan')}}/"+$(this).val(),function(data){
                    $('#kecamatan').html(data).val("").trigger('change');
                });
            }else{
                $('#kecamatan').prop('disabled',true).val("").trigger('change');
            }
        });

        // when kecamatan changed
        $('#kecamatan').change(function () {
            if($(this).val() != ""){
                $('#desa').prop('disabled',false);
                $.get("{{url('backoffice/kost/get_desa')}}/"+$(this).val(),function(data) {
                    $('#desa').html(data).val("").trigger('change');
                });
            }else{
                $('#desa').prop('disabled',true).val("").trigger('change');
            }
        });

        $('#btn-add-fasilitas').click(function(e){
            e.preventDefault(e);
            var btn = $(this);
            var txt = btn.parent().prev();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{!! csrf_token() !!}",
                },
                url : "{{route('bo.kost.add_fasilitas')}}",
                data : "fas="+txt.val(),
                dataType : "json",
                type : "post",
                success : function(resp){
                    $("#fasilitas").select2("destroy");
                    txt.val("");
                    $("#fasilitas").html(resp.data);
                    $("#fasilitas").select2();
                    console.log(resp);
                }
            });
        });

        // jenis sewa UX
        $(document).on('change','.jenis',function(e){
            var vl = $(this);
            if(vl.val() == 'bulanan'){
                vl.parent().next().find('.lbl').html('bulan');
                vl.parent().parent().find('.harga-sewa').attr('placeholder','Harga / bulan');
            }else if(vl.val() == 'tahunan'){
                vl.parent().next().find('.lbl').html('tahun');
                vl.parent().parent().find('.harga-sewa').attr('placeholder','Harga / tahun');
            }else{
                vl.parent().next().find('.lbl').html('hari');
                vl.parent().parent().find('.harga-sewa').attr('placeholder','Harga / hari');
            }
        });

    </script>
@endsection