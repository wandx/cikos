@extends('back._master')

@section('content_caption')
    <h3>
        Dashboard
    </h3>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-danger">
                <div class="box-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active" role="presentation">
                            <a href="#tab1" aria-controls="tab1" data-toggle="tab">
                                <i class="fa fa-download"></i> Inbox
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#tab2" aria-controls="tab2" data-toggle="tab">
                                <i class="fa fa-upload"></i> Outbox
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#tab3" aria-controls="tab3" data-toggle="tab">
                                <i class="fa fa-check-circle-o"></i> Sent
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#tab4" aria-controls="tab4" data-toggle="modal" data-target="#modalSMS">
                                <i class="fa fa-plus"></i> Tulis SMS
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1" role="tabpanel">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Sender</th>
                                    <th>Message</th>
                                    <th>Receive Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($inbox->count() > 0)
                                    @foreach($inbox as $in)
                                        <tr>
                                            <td>{{$in->SenderNumber}}</td>
                                            <td>{{$in->TextDecoded}}</td>
                                            <td>{{$in->ReceivingDateTime}}</td>
                                            <td>{{$in->ID}}</td>
                                        </tr>

                                    @endforeach
                                @else
                                    <tr>
                                        <td class="text-center" colspan="4">No Data</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab2" role="tabpanel">2</div>
                        <div class="tab-pane" id="tab3" role="tabpanel">3</div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    <!-- Modal -->
    <div id="modalSMS" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">SMS Baru</h4>
                </div>
                <br>
                <div class="modal-body">
                    {!! Form::open(['route'=>'bo.sms.send']) !!}
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="bottom" title="Nomer Tujuan">+62</span>
                            {!! Form::number('nope',null,['class'=>'form-control','required'=>true]) !!}
                        </div>
                        <br>
                        <div class="form-group">
                            {!! Form::textarea('msg',null,['class'=>'form-control','rows'=>'4','maxlength'=>160,'placeholder'=>'tulis pesan...']) !!}
                        </div>
                        <br>
                        <div class="form-group">
                            <button class="btn btn-primary">Send</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
@stop

