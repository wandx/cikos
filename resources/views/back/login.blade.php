<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Login Form</title>




    <link rel="stylesheet" href="{{asset('assets/css/login.css')}}">




</head>

<body>

<div class="main-wrap">
    {!! Form::open() !!}
    <div class="login-main">
        {!! Form::email('email',null,['class'=>'box1 border1','placeholder'=>'email']) !!}
        {!! Form::password('password',['class'=>'box1 border2','placeholder'=>'password']) !!}
        <input type="submit" class="send" value="Go">
    </div>
    {!! Form::close() !!}
    @if(count($errors) > 0)
    <div class="login-alert">
        @foreach($errors->all() as $e)
            <li>{{$e}}</li>
        @endforeach
    </div>
    @endif
</div>





</body>
</html>
