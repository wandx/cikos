<?php

namespace App\Http\Controllers\Back;

use App\Models\Inbox;
use App\Models\Outbox;
use App\Models\Section;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SmsCont extends Controller
{
    public function index(){
        Section::where('name','sms_notification')->update(['content'=>'0']);
        $data = [
            'inbox'=>Inbox::orderBy('ReceivingDateTime','DESC')->get()
        ];
        return view('back.sms.index',$data);
    }

    public function send(Request $req){
        $nope = $req->nope;
        $msg = $req->msg;

        $data = [
            'DestinationNumber'=>'+62'.$nope,
            'TextDecoded' => $msg,
            'CreatorID'=>auth()->user()->name
        ];
        Outbox::insert($data);
        return redirect()->back();
    }
}
