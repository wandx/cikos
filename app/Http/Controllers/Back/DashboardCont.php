<?php

namespace App\Http\Controllers\Back;

use App\Models\Desa;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardCont extends Controller
{
    public function jal2(Request $req){
        return $req->image;
    }
    public function login(){
        return view('back.login');
    }

    public function logout(){
        auth()->logout();
        return redirect()->route('bo.login');
    }

    public function do_login(Request $req){
        if(auth()->attempt(['email'=>$req->email,'password'=>$req->password])){
            return redirect()->route('bo.dashboard');
        }
        return redirect()->back()->withInput()->withErrors(['failed'=>'Email or password not match']);
    }

    public function index(){
        return view('back.dashboard.index');
    }

    public function jal(){
        $x = Desa::where('id',1101010007)->with('kecamatan.kabupaten.provinsi')->get();
        return dd($x);
    }
}
