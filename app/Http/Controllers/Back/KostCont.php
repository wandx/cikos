<?php

namespace App\Http\Controllers\Back;

use App\Libraries\Front;
use App\Models\Contact;
use App\Models\Facility;
use App\Models\Kost;
use App\Models\Rent;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AddKostReq;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class KostCont extends Controller
{
    public function add(){
        return view('back.kost.add_kost');
    }

    public function do_add(AddKostReq $req){
        // data kost
        $data_kost = [
            'user_id' => auth()->user()->id,
            'name' => $req->nama_kost,
            'address'=>$req->alamat,
            'desa_id'=>$req->desa,
            'latitude' => $req->latitude,
            'longitude'=>$req->longitude,
            'desc'=>$req->desc
        ];
        $in_kost = Kost::create($data_kost);

        // data kontak
        foreach($req->input('jenis-kontak') as $k=>$v){
            foreach($req->input('nomer') as $k2=>$v2){
                if($k == $k2){
                    // insert kontak
                    $data_kontak = [
                        'contact_type' => $req->input('jenis-kontak.'.$k),
                        'contact_number' => $req->input('nomer.'.$k2)
                    ];
                    $x = Contact::create($data_kontak);
                    // pivot contact - user_profile
                    DB::table('contact_kost')->insert([
                        'kost_id'=> $in_kost->id,
                        'contact_id' => $x->id
                    ]);
                }
            }
        }

        // data harga sewa
        foreach($req->input('jenis-sewa') as $k=>$v){
            foreach ($req->input('harga') as $k2=>$v2){
                foreach($req->input('periode') as $k3=>$v3){
                    if($k == $k2 && $k2 == $k3){
                        // insert sewa
                        $data_sewa = [
                            'jenis_sewa' => $req->input('jenis-sewa.'.$k),
                            'periode' => $req->input('periode.'.$k3),
                            'harga' => $req->input('harga.'.$k2)
                        ];
                        $x = Rent::create($data_sewa);
                        // pivot kost - rent
                        DB::table('kost_rent')->insert([
                            'kost_id' => $in_kost->id,
                            'rent_id' => $x->id
                        ]);
                    }
                }
            }
        }
    }
    
    public function get_kab($provinsi_id,$sel=""){
        return Front::get_kab($provinsi_id,$sel);
    }

    public function get_kec($kabupaten_id,$sel=""){
        return Front::get_kec($kabupaten_id,$sel);
    }

    public function get_desa($kecamatan_id,$sel=""){
        return Front::get_desa($kecamatan_id,$sel);
    }

    // Add fasilitas
    public function add_fasilitas(Request $req){
        Facility::create(['name'=>$req->fas]);
        $x = Facility::all();
        $li = "<option value=''>Pilih fasilitas</option>";
        foreach ($x as $dt){
            $li .= "<option value='".$dt->id."'>".$dt->name."</option>";
        }
        return ['status'=>'ok','data'=>$li];
    }

    // upload img
    public function upload_img(Request $req){
        $x = rand(0,999999999999999).'.jpg';
        $temp = 'uploads/temp_img/';
        $dir = rand(0,9999);
        $new_temp = $temp.$dir.'/';

        if($req->session()->get('dir') == null){
            if(!is_dir($new_temp)){
                mkdir($new_temp);
                $req->session()->set('dir',$new_temp);
            }
            Image::make($req->img)->save($new_temp.$x);
        }else{
            Image::make($req->img)->save($req->session()->get('dir').$x);
        }



        return ['success'=>true,'filename'=>$x];
    }
}
