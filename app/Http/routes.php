<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace'=>'Back','prefix'=>'backoffice'],function(){
    Route::group(['middleware'=>'guest'],function(){
        // Login form
        Route::get('/',['as'=>'bo.login','uses'=>'DashboardCont@login']);
        Route::post('/',['as'=>'bo.login.post','uses'=>'DashboardCont@do_login']);
    });

    Route::group(['middleware'=>['backoffice','auth']],function(){
        // Dashboard
        Route::get('dashboard',['as'=>'bo.dashboard','uses'=>'DashboardCont@index']);

        // Kost
        Route::group(['prefix'=>'kost'],function(){
            Route::get('kost',['as'=>'bo.kost.lists','uses'=>'KostCont@lists']);
            // Add
            Route::get('add_kost',['as'=>'bo.kost.add','uses'=>'KostCont@add']);
            Route::post('add_kost',['as'=>'bo.kost.add.post','uses'=>'KostCont@do_add']);

            // upload to temp
            Route::post('upload_img',['as'=>'bo.kost.upload','uses'=>'KostCont@upload_img']);

            // Wilayah
            Route::get('get_kabupaten/{provinsi_id}/{sel?}','KostCont@get_kab');
            Route::get('get_kecamatan/{kabupaten_id}/{sel?}','KostCont@get_kec');
            Route::get('get_desa/{kecamatan_id}/{sel?}','KostCont@get_desa');

            // Add fasilitas
            Route::post('add_fasilitas',['as'=>'bo.kost.add_fasilitas','uses'=>'KostCont@add_fasilitas']);
            
            Route::get('edit_kost/{id}',['as'=>'bo.kost.edit','uses'=>'KostCont@edit']);
            Route::post('update_kost',['as'=>'bo.kost.update','uses'=>'KostCont@update']);
            Route::delete('delete_kost',['as'=>'bo.kost.delete','uses'=>'KostCont@delete']);

            Route::get('jal',function(){

                $redis = LRedis::connection();
                $data = ['message'=>\App\Models\Section::where('name','sms_notification')->first()->content];
                $redis->publish('message', json_encode($data));
                return response()->json([]);
            });
        });

        Route::group(['prefix'=>'sms'],function(){
            Route::get('/',['as'=>'bo.sms','uses'=>'SmsCont@index']);

            // send
            Route::post('send',['as'=>'bo.sms.send','uses'=>'SmsCont@send']);
        });

        
        // Logout
        Route::get('logout',['as'=>'bo.logout','uses'=>'DashboardCont@logout']);
        Route::get('jal','DashboardCont@jal');
    });
});


