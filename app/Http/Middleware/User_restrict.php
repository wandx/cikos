<?php

namespace App\Http\Middleware;

use App\Models\Menu;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\DB;

class User_restrict
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param $menu_name
     * @return mixed
     */

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next,$menu_name)
    {
        $menu_id = Menu::where('menu_name',$menu_name)->get()->first()->id;
        $cek = DB::table('menu_user')->where('user_id',auth()->user()->id)->where('menu_id',$menu_id)->count();
        //$cek = User_access_menu::where('user_id',auth()->user()->id)->where('menu_id',$menu_id)->count();
        if($cek == 0){
            return abort(403, 'Unauthorized action.');
        }
        return $next($request);
    }
}
