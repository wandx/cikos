<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AddKostReq extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nama_kost' => 'required',
            'prov' => 'required',
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'desa' => 'required',
            'alamat' => 'required',
            'gender' => 'required',
            'latitude' => 'required|numeric',
            'longitude'=> 'required|numeric',
            'desc' => 'required'
        ];
        // jenis kontak
        foreach($this->request->get('jenis-kontak') as $key=>$val){
            $rules['jenis-kontak.'.$key] = 'required';
        }
        // nomer kontak
        foreach($this->request->get('nomer') as $key=>$val){
            $rules['nomer.'.$key] = 'required|numeric';
        }

        // jenis sewa
        foreach($this->request->get('jenis-sewa') as $key=>$val){
            $rules['jenis-sewa.'.$key] = 'required';
        }
        // harga sewa
        foreach($this->request->get('harga') as $key=>$val){
            $rules['harga.'.$key] = 'required|numeric';
        }
        // periode sewa
        foreach($this->request->get('periode') as $key=>$val){
            $rules['periode.'.$key] = 'required|numeric';
        }

        return $rules;
    }

    public function messages(){
        return [
            'nama_kost.required' => 'Nama kost harus diisi',
            'prov.required' => 'Provinsi harus diisi',
            'kabupaten.required'=>'Kabupaten harus diisi',
            'kecamatan.required'=>'Kecamatan harus diisi',
            'desa.required'=>'Desa harus diisi',
            'alamat.required'=>'Alamat harus diisi',
            'gender.required'=>'Gender harus diisi',
            'nomer.required' => 'Nomer telfon harus diisi',
            'nomer.numeric' => 'Hanya angka'
        ];
    }
}

