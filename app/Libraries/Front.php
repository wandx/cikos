<?php

namespace App\Libraries;

use App\Models\Balance;
use App\Models\Bank;
use App\Models\Desa;
use App\Models\Facility;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Provinsi;
use App\Models\Section;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Facades\Facade;

class Front extends Facade{
    protected static function getFacadeAccessor()
    {
        return 'front';
    }
    
    public static function get_section($section_name){
        $sec = Section::where('section_name',$section_name)->first();
        return ($sec->count() > 0) ? $sec->section_content:'';
    }

    public static function bank_name($bank_id){
        $x = Bank::where('id',$bank_id)->first();
        return $x->bank_name;
    }

    public static function is_selected($all,$val){
        if(in_array($val,$all)){
            return true;
        }
        return false;
    }

    public static function compare($sel,$all){
        // add sel with default value = 0
        foreach($all as $k=>$v){
            $all[$k]['sel'] = 0;
        }

        // gett similar value
        foreach($all as $k=>$v){
            foreach($sel as $k2=>$v2){
                // when match change sel to 1
                if($v['id'] == $v2['id']){
                    $all[$k]['sel'] = 1;
                }

            }
        }
        return $all;
    }

    public static function count($type){
        switch ($type){
            case 'vendor':

                $c =  number_format(User::where('role','Vendor')->where('confirmed',1)->where('suspend',0)->count(),0,',','.');
                break;
            case 'member':
                $c =  number_format(User::where('role','User')->where('confirmed',1)->where('suspend',0)->count(),0,',','.');
                break;
            case 'total_saldo' :
                $c = number_format(Balance::sum('balance_amount'),0,',','.');
                break;
            case 'total_transaksi' :
                $c = number_format(Transaction::sum('transaction_amount'),0,',','.');
                break;
            default :
                $c = 0;
                break;
        }

        return $c;
    }

    public static function get_prov($sel=""){
        $x = Provinsi::all();
        $li = '<option value="">Pilih Provinsi</option>';
        foreach ($x as $dt){
            if($sel == $dt->id){
                $li .= '<option selected value="'.$dt->id.'">'.$dt->nama.'</option>';
            }else{
                $li .= '<option value="'.$dt->id.'">'.$dt->nama.'</option>';
            }
        }
        return $li;
    }

    public static function get_kab($provinsi_id,$sel=""){
        $x = Kabupaten::where('provinsi_id',$provinsi_id)->get();
        $li = '<option value="">Pilih Kabupaten</option>';
        foreach ($x as $dt){
            if($sel == $dt->id){
                $li .= '<option selected value="'.$dt->id.'">'.$dt->nama.'</option>';
            }else{
                $li .= '<option value="'.$dt->id.'">'.$dt->nama.'</option>';
            }

        }
        return $li;
    }

    public static function get_kec($kabupaten_id,$sel=""){
        $x = Kecamatan::where('kabupaten_id',$kabupaten_id)->get();
        $li = '<option value="">Pilih Kecamatan</option>';
        foreach ($x as $dt){
            if($sel == $dt->id){
                $li .= '<option selected value="'.$dt->id.'">'.$dt->nama.'</option>';
            }else{
                $li .= '<option value="'.$dt->id.'">'.$dt->nama.'</option>';
            }
        }
        return $li;
    }

    public static function get_desa($kecamatan_id,$sel=""){
        $x = Desa::where('kecamatan_id',$kecamatan_id)->get();
        $li = '<option value="">Pilih Desa</option>';
        foreach ($x as $dt){
            if($sel == $dt->id){
                $li .= '<option selected value="'.$dt->id.'">'.$dt->nama.'</option>';
            }else{
                $li .= '<option value="'.$dt->id.'">'.$dt->nama.'</option>';
            }
        }
        return $li;
    }


    // fasilitas
    public static function fasilitas(){
        $x = Facility::all();
        $li = "<option value=''>Pilih fasilitas</option>";
        foreach ($x as $dt){
            $li .= "<option value='".$dt->id."'>".$dt->name."</option>";
        }
        return $li;
    }
}