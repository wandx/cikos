<?php
    
    namespace App\Libraries;

    use App\Models\Bank;
    use App\Models\Bank_confirmation;
    use App\Models\Menu;
    use App\Models\Section;
    use App\Models\Sub_menu;
    use App\Models\User_access_menu;
    use App\Models\User;
    use Illuminate\Support\Facades\DB;

    class Admin{
        public static function menu(){
            $user_id = auth()->user()->id;
            $data = [
                'user' => User::where('id',$user_id)->where('role','admin')
                                                    ->with(['menu'=>function($q){
                                                        $q->with('sub_menu');
                                                    }])->get()
            ];
            
            return view('back.partial.menu',$data);
        }

        public static function check_sms_notification(){
            $x = Section::where('name','sms_notification')->first();
            if($x->content > 0){
                return true;
            }
            return false;
        }
    }

