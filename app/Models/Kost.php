<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kost extends Model
{
    protected $guarded = ['id'];
    
    // kost punya banyak jenis sewa
    public function rent(){
        return $this->belongsToMany(Rent::class);
    }
    
    // kost punya banyak gambar
    public function picture(){
        return $this->belongsToMany(Picture::class);
    }
    
    // kost punya banyak fasilitas
    public function facility(){
        return $this->belongsToMany(Facility::class);
    }

    // kost punya desa
    public function desa(){
        return $this->hasOne(Desa::class);
    }

    // kost punya kontak
    public function contact(){
        return $this->belongsToMany(Contact::class);
    }
}
