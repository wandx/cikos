<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'wilayah_kecamatan';
    public $timestamps = false;

    public function kabupaten(){
        return $this->belongsTo(Kabupaten::class);
    }

    public function desa(){
        return $this->hasMany(Desa::class);
    }
}
