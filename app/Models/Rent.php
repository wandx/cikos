<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    public function kost(){
        return $this->belongsToMany(Kost::class);
    }
}
