<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    protected $table='inbox';
    protected $primaryKey='ID';
    protected $guarded = ['ID'];
    public $timestamps = false;

}
