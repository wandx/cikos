<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public $timestamps = false;
    protected $guarded = ['id'];

    public function user(){
        return $this->belongsToMany(User::class);
    }

    public function sub_menu(){
        return $this->hasMany(Sub_menu::class);
    }
}
