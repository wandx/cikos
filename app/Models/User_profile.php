<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_profile extends Model
{
    public $timestamps = false;
    protected $guarded = ['id'];

    public function social_media(){
        return $this->belongsToMany(Social_media::class);
    }

    public function contact(){
        return $this->belongsToMany(Contact::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
