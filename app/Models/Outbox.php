<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Outbox extends Model
{
    protected $table='outbox';
    protected $primaryKey='ID';
    protected $guarded = ['ID'];
    public $timestamps = false;
}
