<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sub_menu extends Model
{
    public $timestamps = false;
    protected $guarded = ['id'];

    public function menu(){
        return $this->belongsTo(Menu::class);
    }
}
