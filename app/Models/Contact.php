<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $timestamps = false;
    protected $guarded = ['id'];

    public function user_profile(){
        return $this->belongsToMany(User_profile::class);
    }

    public function kost(){
        return $this->belongsToMany(Kost::class);
    }
}
