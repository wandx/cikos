<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function menu(){
        return $this->belongsToMany(Menu::class);
    }
    
    public function user_profile(){
        return $this->hasOne(User_profile::class);
    }

    public function is_admin(){
        if($this->role == 'admin'){
            return true;
        }
        return false;
    }
}
