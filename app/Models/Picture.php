<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $guarded = ['id'];
    
    public function kost(){
        return $this->belongsToMany(Kost::class);
    }
}
