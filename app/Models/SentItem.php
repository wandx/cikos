<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SentItem extends Model
{
    protected $table='sentitems';
    protected $primaryKey='ID';
    protected $guarded = ['ID'];
    public $timestamps = false;
}
