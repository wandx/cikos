<?php

namespace App\Console\Commands;

use App\Models\Section;
use Illuminate\Console\Command;
use LRedis;

class SmsNotifyConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify if sms received';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $x = Section::where('name','sms_notification');
        $data = [
            'content'=> ($x->first()->content + 1)
        ];
        $x->update($data);

        $redis = LRedis::connection();
        $data = ['sms_notif'=>Section::where('name','sms_notification')->first()->content];
        $redis->publish('message', json_encode($data));
        $this->info('Notification added');
        return response()->json([]);
    }
}
